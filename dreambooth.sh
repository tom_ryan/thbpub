#!/bin/sh

mkdir ~/.ssh
chmod 700 ~/.ssh
echo $PUBLIC_KEY >> ~/.ssh/authorized_keys # PUBLIC_KEY is a runpod var
chmod 700 ~/.ssh/authorized_keys

apt-get update

# openssh for runpod
DEBIAN_FRONTEND=noninteractive apt-get install -y openssh-server 
service ssh start

# automatic1111 deps
DEBIAN_FRONTEND=noninteractive apt-get install -y wget git python3 python3-venv sudo
useradd -d /workspace/automatic1111 automatic1111
wget -qO- https://raw.githubusercontent.com/AUTOMATIC1111/stable-diffusion-webui/master/webui.sh | sudo -u automatic1111 install_dir=/workspace/automatic1111 bash

sleep infinity
